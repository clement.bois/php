# GitLab CI template for PHP

This project implements a GitLab CI/CD template to build, test and analyse your [PHP](https://www.php.net/) projects.

:warning: For now it only supports [Composer](https://getcomposer.org/) as dependency manager.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/php/gitlab-ci-php@4.6.0
    # 2: set/override component inputs
    inputs:
      image: "registry.hub.docker.com/library/php:8-apache" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/php'
    ref: '4.6.0'
    file: '/templates/gitlab-ci-php.yml'

variables:
  # 2: set/override template variables
  PHP_IMAGE: "registry.hub.docker.com/library/php:8-apache" # ⚠ this is only an example
```

## Global configuration

The PHP template uses some global configuration used throughout all jobs.

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `image` / `PHP_IMAGE` | The Docker image used to run PHP <br/>:warning: **set the version required by your project** | `registry.hub.docker.com/library/php:latest` |
| `project-dir` / `PHP_PROJECT_DIR` | The PHP project root directory         | `.`               |

## Managing PHP extensions

Depending on the PHP image you'll be using, your project might require PHP extensions not already installed/enabled in
the image.

For this, you may use the following files (located in `PHP_PROJECT_DIR`):

* `.php_packages` containing required system libraries (in a single line),
* `.php_pecl` containing required PECL extensions (in a single line),
* `.php_ext` containing required PHP extensions (in a single line).

More info [in _How to install more PHP extensions_ chapter](https://hub.docker.com/_/php/).

Example: a project requires `php-gd` and `php-zip` extensions (themselves requiring `libzip-dev` and `libpng-dev` libraries)

`.php_packages`:

```text
libzip-dev libpng-dev
```

`.php_ext`:

```text
gd zip
```

## Jobs

### `php-unit` job

This job performs [PHPUnit](https://docs.phpunit.de/) tests.

It is bound to the `build` stage, and is enabled by default.

It uses the following variables:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `unit-args` / `PHP_UNIT_ARGS` | Additional PHPUnit [options](https://docs.phpunit.de/en/11.0/textui.html#command-line-options) | _none_ |
| `unit-disabled` / `PHP_UNIT_DISABLED` | Set to `true` to disable PHPUnit test | _none_ (auto based on presence of `phpunit.xml` or `phpunit.xml.dist` file) |

:warning: in order to be able to compute [code coverage](https://docs.phpunit.de/en/11.0/code-coverage.html),
your project shall have a (dev) dependency to [`php-code-coverage`](https://github.com/sebastianbergmann/php-code-coverage).

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$PHP_PROJECT_DIR/reports/php-test.xunit.xml` | [xUnit](https://en.wikipedia.org/wiki/XUnit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) & [SonarQube integration](https://docs.sonarqube.org/latest/analysis/test-coverage/test-execution-parameters/#header-7) |
| `$PHP_PROJECT_DIR/reports/php-coverage.cobertura.xml` | [Cobertura XML](https://gcovr.com/en/stable/output/cobertura.html) coverage report | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscoverage_report) |
| `$PHP_PROJECT_DIR/reports/php-coverage.clover.xml` | [Clover XML](https://openclover.org/) coverage report | [SonarQube integration](https://docs.sonarqube.org/latest/analysis/test-coverage/test-coverage-parameters/#header-9) |

### `php-codesniffer` job

This job performs a [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) analysis of your code.

It is bound to the `test` stage, and is **enabled by default**.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| -------------------------- | ---------------------------------------- | ----------------- |
| `codesniffer-disabled` / `PHP_CODESNIFFER_DISABLED` | Set to `true` to disable this job                  | _none_ (enabled)  |
| `codesniffer-args` / `PHP_CODESNIFFER_ARGS` | PHP_CodeSniffer [options](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Configuration-Options) | _none_ |

You have two options to configure PHP_CodeSniffer for your project:

* either override the `$PHP_CODESNIFFER_ARGS` variable with your desired options,
* or use an [XML configuration file](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file)
  located in `PHP_PROJECT_DIR` (`.phpcs.xml`, `phpcs.xml`, `.phpcs.xml.dist`, or `phpcs.xml.dist`).

:bulb: When issues are found, don't hesitate to use [`phpcbf`](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically#using-the-php-code-beautifier-and-fixer)
to automatically fix them. Or even better [php-cs-fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer).

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$PHP_PROJECT_DIR/reports/php-codesniffer.checkstyle.xml` | [Checkstyle](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting#printing-a-checkstyle-report) code quality | N/A |

### SonarQube analysis

If you're using the SonarQube template to analyse your PHP code, here is a sample `sonar-project.properties` file.

```properties
# see: https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/php-test-coverage/
sonar.sources=src
sonar.tests=tests

# tests report: xUnit format
sonar.php.tests.reportPath=reports/php-test.xunit.xml
# coverage report: Clover format
sonar.php.coverage.reportPaths=reports/php-coverage.clover.xml
```

More info:

* [PHP language support](https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/php-test-coverage/)
* [test coverage & execution parameters](https://docs.sonarqube.org/latest/analysis/coverage/)
* [third-party issues](https://docs.sonarqube.org/latest/analysis/external-issues/)

### `php-sbom` job

This job generates a [SBOM](https://cyclonedx.org/) file listing installed packages using [@cyclonedx/cyclonedx-php](https://github.com/CycloneDX/cyclonedx-php-composer).

It is bound to the `test` stage, and uses the following variables:

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `sbom-disabled` / `PHP_SBOM_DISABLED` | Set to `true` to disable this job | _none_ |
| `sbom-version` / `PHP_SBOM_VERSION` | The version of @cyclonedx/cyclonedx-php used to emit SBOM | _none_ (uses latest) |
| `sbom-opts` / `PHP_SBOM_OPTS` | [@cyclonedx/cyclonedx-php options](https://github.com/CycloneDX/cyclonedx-php-composer#usage) used for SBOM analysis | _none_ |

### `php-outdated` job

This job shows the list of installed packages that have updates available (uses [`composer outdated`](https://getcomposer.org/doc/03-cli.md#outdated)).

It is bound to the `test` stage, and can be run manually at will.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| ------------------- | ---------------------------------------- | ----------------- |
| `outdated-opts` / `PHP_OUTDATED_OPTS` | [`composer outdated` options](https://getcomposer.org/doc/03-cli.md#outdated) | `--direct` |

### `php-composer-audit` job

This job performs a vulnerability scan in your dependencies with [`composer audit`](https://getcomposer.org/doc/03-cli.md#audit).

It is bound to the `test` stage, and uses the following variables:

| Input / Variable | Description                              | Default value     |
| ------------------- | ---------------------------------------- | ----------------- |
| `composer-audit-disabled` / `PHP_COMPOSER_AUDIT_DISABLED` | Set to `true` to disable this job | _none_ (enabled) |
| `composer-audit-opts` / `PHP_COMPOSER_AUDIT_OPTS` | [`composer audit` options](https://getcomposer.org/doc/03-cli.md#audit) | `--locked` |
